﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Repository
{
	class RepositoryTeam : ITeamRepository
	{

		private ProjectStructureContext db;

		public RepositoryTeam(ProjectStructureContext context)
		{
			db = context;
		}

		public async Task<IEnumerable<User>> GetAll()
		{
			return await db.Set<User>().ToListAsync();
		}

		public async Task<User> Get(int id)
		{
			return await db.Set<User>().Where(p => p.Id == id).FirstAsync();
		}

		public async System.Threading.Tasks.Task Create(User teamToCreate)
		{
			if (teamToCreate != null)
			{
				if (await db.Set<User>().FirstOrDefaultAsync(p => p.Id == teamToCreate.Id) == null)
				{
					await db.Set<User>().AddAsync(teamToCreate);
				}
			}
		}

		public async System.Threading.Tasks.Task Update(int id, User teamToUpdate)
		{
			if (teamToUpdate != null)
			{
				if (await db.Set<User>().FirstOrDefaultAsync(p => p.Id == teamToUpdate.Id) != null)
				{
					var projectToDelete = db.Set<User>().Where(p => p.Id == id).First();
					db.Set<User>().Remove(projectToDelete);
					await db.Set<User>().AddAsync(teamToUpdate);
				}
			}
		}

		public async System.Threading.Tasks.Task Delete(User team)
		{
			if (team != null)
			{
				if (await db.Set<User>().FirstOrDefaultAsync(p => p.Id == team.Id) != null)
				{
					var teamToDelete = db.Set<User>().Where(p => p.Id == team.Id).First();
					db.Set<User>().Remove(teamToDelete);
				}
			}
		}
	}
}
