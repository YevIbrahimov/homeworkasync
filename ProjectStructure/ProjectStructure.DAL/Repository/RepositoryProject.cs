﻿using System.Collections.Generic;
using System.Linq;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.Interfaces;
using System.Threading.Tasks;
using System.Net.Http;
using ProjectStructure.DAL.Context;
using Microsoft.EntityFrameworkCore;

namespace ProjectStructure.DAL.Repository
{
	class RepositoryProject : IProjectRepository
	{
		private ProjectStructureContext db;

		public RepositoryProject(ProjectStructureContext context)
		{
			db = context;
		}

		public async Task<IEnumerable<Project>> GetAll()
		{
			return await db.Set<Project>().ToListAsync();
		}

		public async Task<Project> Get(int id)
		{
			return await db.Set<Project>().Where(p => p.Id == id).FirstAsync();
		}

		public async System.Threading.Tasks.Task Create(Project project)
		{
			if (project != null)
			{
				if (await db.Set<Project>().FirstOrDefaultAsync(p => p.Id == project.Id) == null)
				{
					await db.Set<Project>().AddAsync(project);
				}
			}
		}

		public async System.Threading.Tasks.Task Update(int id, Project projectToUpdate)
		{
			if (projectToUpdate != null)
			{
				if (await db.Set<Project>().FirstOrDefaultAsync(p => p.Id == projectToUpdate.Id) != null)
				{
					var projectToDelete = db.Set<Project>().Where(p => p.Id == id).First();
					db.Set<Project>().Remove(projectToDelete);
					await db.Set<Project>().AddAsync(projectToUpdate);
				}
			}
		}

		public async System.Threading.Tasks.Task Delete(Project project)
		{
			if (project != null)
			{
				if (await db.Set<Project>().FirstOrDefaultAsync(p => p.Id == project.Id) != null)
				{
					var projectToDelete = db.Set<Project>().Where(p => p.Id == project.Id).First();
					db.Set<Project>().Remove(projectToDelete);
				}
			}
		}

	}
}
