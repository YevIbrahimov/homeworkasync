﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Repository
{
	class RepositoryTask
	{
		private ProjectStructureContext db;

		public RepositoryTask(ProjectStructureContext context)
		{
			db = context;
		}

		public async Task<IEnumerable<Models.Task>> GetAll()
		{
			return await db.Set<Models.Task>().ToListAsync();
		}

		public async Task<Models.Task> Get(int id)
		{
			return await db.Set<Models.Task>().Where(p => p.Id == id).FirstAsync();
		}

		public async System.Threading.Tasks.Task Create(Models.Task task)
		{
			if (task != null)
			{
				if (await db.Set<Models.Task>().FirstOrDefaultAsync(p => p.Id == task.Id) == null)
				{
					await db.Set<Models.Task>().AddAsync(task);
				}
			}
		}

		public async System.Threading.Tasks.Task Update(int id, Models.Task taskToUpdate)
		{
			if (taskToUpdate != null)
			{
				if (await db.Set<Models.Task>().FirstOrDefaultAsync(p => p.Id == taskToUpdate.Id) != null)
				{
					var projectToDelete = db.Set<Models.Task>().Where(p => p.Id == id).First();
					db.Set<Models.Task>().Remove(taskToUpdate);
					await db.Set<Models.Task>().AddAsync(taskToUpdate);
				}
			}
		}

		public async System.Threading.Tasks.Task Delete(Models.Task task)
		{
			if (task != null)
			{
				if (await db.Set<Models.Task>().FirstOrDefaultAsync(p => p.Id == task.Id) != null)
				{
					var taskToDelete = db.Set<Models.Task>().Where(p => p.Id == task.Id).First();
					db.Set<Models.Task>().Remove(taskToDelete);
				}
			}
		}
	}
}
