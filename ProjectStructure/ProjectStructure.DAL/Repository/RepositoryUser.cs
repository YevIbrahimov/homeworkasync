﻿using System;
using ProjectStructure.DAL.Models;
using System.Collections.Generic;
using System.Linq;
using ProjectStructure.DAL.Context;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ProjectStructure.DAL.Repository
{
	class RepositoryUser
	{

		private ProjectStructureContext db;

		public RepositoryUser(ProjectStructureContext context)
		{
			db = context;
		}

		public async Task<IEnumerable<User>> GetAll()
		{
			return await db.Set<User>().ToListAsync();
		}

		public async Task<User> Get(int id)
		{
			return await db.Set<User>().Where(p => p.Id == id).FirstAsync();
		}

		public async System.Threading.Tasks.Task Create(User userToCreate)
		{
			if (userToCreate != null)
			{
				if (await db.Set<User>().FirstOrDefaultAsync(p => p.Id == userToCreate.Id) == null)
				{
					await db.Set<User>().AddAsync(userToCreate);
				}
			}
		}

		public async System.Threading.Tasks.Task Update(int id, User userToUpdate)
		{
			if (userToUpdate != null)
			{
				if (await db.Set<User>().FirstOrDefaultAsync(p => p.Id == userToUpdate.Id) != null)
				{
					var userToDelete = db.Set<User>().Where(p => p.Id == id).First();
					db.Set<User>().Remove(userToDelete);
					await db.Set<User>().AddAsync(userToUpdate);
				}
			}
		}

		public async System.Threading.Tasks.Task Delete(User user)
		{
			if (user != null)
			{
				if (await db.Set<User>().FirstOrDefaultAsync(p => p.Id == user.Id) != null)
				{
					var userToDelete = db.Set<User>().Where(p => p.Id == user.Id).First();
					db.Set<User>().Remove(userToDelete);
				}
			}
		}
	}
}
