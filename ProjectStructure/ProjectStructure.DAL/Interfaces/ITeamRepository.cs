﻿using ProjectStructure.DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Interfaces
{
	public interface ITeamRepository
	{
		Task<IEnumerable<User>> GetAll();

		Task<User> Get(int id);

		System.Threading.Tasks.Task Create(User team);

		System.Threading.Tasks.Task Update(int id, User team);

		System.Threading.Tasks.Task Delete(User team);
	}
}
