﻿using ProjectStructure.DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Interfaces
{
	public interface IProjectRepository
	{
		Task<IEnumerable<Project>> GetAll();

		Task<Project> Get(int id);

		System.Threading.Tasks.Task Create(Project project);

		System.Threading.Tasks.Task Update(int id, Project project);

		System.Threading.Tasks.Task Delete(Project project);
	}
}
