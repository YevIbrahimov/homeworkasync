﻿using ProjectStructure.DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Interfaces
{
	public interface IUserRepository
	{
		Task<IEnumerable<User>> GetAll();

		Task<User> Get(int id);

		System.Threading.Tasks.Task Create(User user);

		System.Threading.Tasks.Task Update(int id, User user);

		System.Threading.Tasks.Task Delete(User user);
	}
}
