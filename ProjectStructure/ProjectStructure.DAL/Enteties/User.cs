﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Models
{
	public class User
	{
		public User()
		{
		}

		public User(int id, int? teamId, string firstName, string lastName, string email, DateTime registeredAt, DateTime birthDay)
		{
			Id = id;
			TeamId = teamId;
			FirstName = firstName;
			LastName = lastName;
			Email = email;
			RegisteredAt = registeredAt;
			BirthDay = birthDay;
		}

		public int Id { get; set; }
		public int? TeamId { get; set; }
		public User Team { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public DateTime RegisteredAt { get; set; }
		public DateTime BirthDay { get; set; }
		//public List<Task> Tasks { get; set; }
	}
}
