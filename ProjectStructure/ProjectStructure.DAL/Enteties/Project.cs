﻿using System;


namespace ProjectStructure.DAL.Models
{
	public class Project
	{
		public Project()
		{
		}

		public Project(int id, string name, int teamId, int authorId, DateTime createdAt, DateTime deadline, string description)
		{
			Id = id;
			Name = name;
			TeamId = teamId;
			AuthorId = authorId;
			CreatedAt = createdAt;
			Deadline = deadline;
			Description = description;
		}

		public int Id { get; set; }
		public int AuthorId { get; set; }
		public User Author { get; set; }
		public int TeamId { get; set; }
		public User Team { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public DateTime Deadline { get; set; }
		public DateTime CreatedAt { get; set; }

	}
}
