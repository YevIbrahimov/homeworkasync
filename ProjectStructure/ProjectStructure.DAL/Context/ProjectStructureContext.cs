﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.DAL.Context
{
	public class ProjectStructureContext: DbContext
	{
        public ProjectStructureContext(DbContextOptions<ProjectStructureContext> options) : base(options)
        { }
        DbSet<Project> Projects { get; set; }
        DbSet<Task> Tasks { get; set; }
        DbSet<User> Teams { get; set; }
        DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>();
            modelBuilder.Entity<Task>();
            modelBuilder.Entity<User>();
            modelBuilder.Entity<User>();
        }

        public ProjectStructureContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=Projects;Trusted_Connection=True;");
        }
    }
}
