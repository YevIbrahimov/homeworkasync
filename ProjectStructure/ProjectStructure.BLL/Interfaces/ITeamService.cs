﻿using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
	public interface ITeamService
	{
		Task<IEnumerable<TeamDTO>> GetAll();
		Task<TeamDTO> Get(int id);
		Task Create(TeamDTO teamDTO);
		Task Update(TeamDTO teamDTO);
		Task Delete(int id);
	}
}