﻿using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
	public interface IUserService
	{
		Task<IEnumerable<UserDTO>> GetAll();
		Task<UserDTO> Get(int id);
		Task Create(UserDTO userDTO);
		Task Update(UserDTO userDTO);
		Task Delete(int id);
	}
}
