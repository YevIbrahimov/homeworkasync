﻿using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
	public interface IProjectService
	{
		Task<IEnumerable<ProjectDTO>> GetAll();
		Task<ProjectDTO> Get(int id);
		Task Create(ProjectDTO projectDTO);
		Task Update(ProjectDTO projectDTO);
		Task Delete(int id);
	}
}
