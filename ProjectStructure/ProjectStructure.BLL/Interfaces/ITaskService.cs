﻿using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
	public interface ITaskService
	{
		Task<IEnumerable<TaskDTO>> GetAll();
		Task<TaskDTO> Get(int id);
		Task Create(TaskDTO taskDTO);
		Task Update(TaskDTO taskDTO);
		Task Delete(int id);
	}
}
