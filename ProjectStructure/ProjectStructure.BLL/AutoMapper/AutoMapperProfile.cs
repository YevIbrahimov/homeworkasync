﻿using AutoMapper;
using ProjectStructure.BLL.DTO;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BLL.AutoMapper
{
	class AutoMapperProfile : Profile
	{
		public AutoMapperProfile()
		{
			CreateMap<Project, TaskDTO>();
			CreateMap<TaskDTO, Project>();
			CreateMap<User, UserDTO>();
			CreateMap<UserDTO, User>();
			CreateMap<Task, TaskDTO>();
			CreateMap<TaskDTO, Task>();
			CreateMap<User, UserDTO>();
			CreateMap<UserDTO, User>();
		}
	}
}
