﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.DTO;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
	public class UserService : IUserService
	{
		private readonly IUnitOfWork data;
		private readonly IMapper mapper;
		private ProjectStructureContext db;

		public UserService(IUnitOfWork data, IMapper mapper, ProjectStructureContext context)
		{
			this.data = data;
			this.mapper = mapper;
			db = context;
		}

		public async Task<IEnumerable<UserDTO>> GetAll()
		{
			return await db.Set<UserDTO>().ToListAsync();
		}

		public async Task<UserDTO> Get(int id)
		{
			return await db.Set<UserDTO>().SingleAsync(e => e.Id == id);
		}

		public async Task Create(UserDTO entity)
		{
			await db.Set<UserDTO>().AddAsync(entity);
		}

		public async Task Update(UserDTO entity)
		{
			if (entity != null)
			{
				var checkProject = await db.Set<UserDTO>().FirstOrDefaultAsync(p => p.Id == entity.Id);
				if (checkProject != null)
				{
					db.Set<UserDTO>().Update(entity);
				}
			}
		}

		public async Task Delete(int id)
		{
			var project = await db.Set<TaskDTO>().FirstOrDefaultAsync(p => p.Id == id);
			if (project != null)
			{
				db.Set<TaskDTO>().Remove(project);
			}
		}
	}
}
