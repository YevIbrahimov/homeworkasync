﻿using ProjectStructure.DAL.Interfaces;
using AutoMapper;
using ProjectStructure.DAL.Context;
using System.Linq;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.DTO;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Services
{
	public class ProjectService : IProjectService
	{
		private readonly IUnitOfWork data;
		private readonly IMapper mapper;
		private ProjectStructureContext db;

		public ProjectService(IUnitOfWork data, IMapper mapper, ProjectStructureContext context)
		{
			this.data = data;
			this.mapper = mapper;
			db = context;
		}

		public async Task<IEnumerable<ProjectDTO>> GetAll()
		{
			return await db.Set<ProjectDTO>().ToListAsync();
		}

		public async Task<ProjectDTO> Get(int id)
		{
			return await db.Set<ProjectDTO>().SingleAsync(e => e.Id == id);
		}

		public async Task Create(ProjectDTO entity)
		{
			if (entity != null)
			{
				if (await db.Set<ProjectDTO>().FirstOrDefaultAsync(p => p.Id == entity.Id) == null)
				{
					await db.Set<ProjectDTO>().AddAsync(entity);
				}
			}
		}

		public async Task Update(ProjectDTO entity)
		{
			if (entity != null)
			{
				var checkProject = await db.Set<ProjectDTO>().FirstOrDefaultAsync(p => p.Id == entity.Id);
				if (checkProject != null)
				{
					db.Set<ProjectDTO>().Update(entity);
				}
			}
		}

		public async Task Delete(int id)
		{
			var project = await db.Set<ProjectDTO>().FirstOrDefaultAsync(p => p.Id == id);
			if (project != null)
			{
				db.Set<ProjectDTO>().Remove(project);
			}
		}
	}
}
