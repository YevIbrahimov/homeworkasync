﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.DTO;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
	class TeamService : ITeamService
	{
		private readonly IUnitOfWork data;
		private readonly IMapper mapper;
		private ProjectStructureContext db;

		public TeamService(IUnitOfWork data, IMapper mapper, ProjectStructureContext context)
		{
			this.data = data;
			this.mapper = mapper;
			db = context;
		}

		public async Task<IEnumerable<TeamDTO>> GetAll()
		{
			return await db.Set<TeamDTO>().ToListAsync();
		}

		public async Task<TeamDTO> Get(int id)
		{
			return await db.Set<TeamDTO>().SingleAsync(e => e.Id == id);
		}

		public async Task Create(TeamDTO entity)
		{
			await db.Set<TeamDTO>().AddAsync(entity);
		}

		public async Task Update(TeamDTO entity)
		{
			if (entity != null)
			{
				var checkProject = await db.Set<TeamDTO>().FirstOrDefaultAsync(p => p.Id == entity.Id);
				if (checkProject != null)
				{
					db.Set<TeamDTO>().Update(entity);
				}
			}
		}

		public async Task Delete(int id)
		{
			var project = await db.Set<TeamDTO>().FirstOrDefaultAsync(p => p.Id == id);
			if (project != null)
			{
				db.Set<TeamDTO>().Remove(project);
			}
		}
	}
}
