﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.DTO;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
	class TaskService : ITaskService
	{
		private readonly IUnitOfWork data;
		private readonly IMapper mapper;
		private ProjectStructureContext db;

		public TaskService(IUnitOfWork data, IMapper mapper, ProjectStructureContext context)
		{
			this.data = data;
			this.mapper = mapper;
			db = context;
		}

		public async Task<IEnumerable<TaskDTO>> GetAll()
		{
			return await db.Set<TaskDTO>().ToListAsync();
		}

		public async Task<TaskDTO> Get(int id)
		{
			return await db.Set<TaskDTO>().SingleAsync(e => e.Id == id);
		}

		public async Task Create(TaskDTO entity)
		{
			await db.Set<TaskDTO>().AddAsync(entity);
		}

		public async Task Update(TaskDTO entity)
		{
			if (entity != null)
			{
				var checkProject = await db.Set<TaskDTO>().FirstOrDefaultAsync(p => p.Id == entity.Id);
				if (checkProject != null)
				{
					db.Set<TaskDTO>().Update(entity);
				}
			}
		}

		public async Task Delete(int id)
		{
			var project = await db.Set<TaskDTO>().FirstOrDefaultAsync(p => p.Id == id);
			if (project != null)
			{
				db.Set<TaskDTO>().Remove(project);
			}
		}
	}
}
