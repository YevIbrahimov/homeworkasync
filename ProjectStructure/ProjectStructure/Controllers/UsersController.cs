﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Models;
using System.Threading.Tasks;

namespace ProjectStructure.WebApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UsersController : Controller
	{
		private readonly IUserService _userService;
		public UsersController(IUserService users)
		{
			_userService = users;
		}

		[HttpGet]
		public async Task<IActionResult> GetUser()
		{
			return Ok(await _userService.GetAll());
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> GetUserById(int id)
		{
			return Ok(await _userService.Get(id));
		}

		[HttpPost]
		public async Task<IActionResult> AddUser(UserDTO userDTO)
		{

			await _userService.Create(userDTO);

			return StatusCode(201, userDTO);
		}

		[HttpPut]
		public async Task<IActionResult> UpdateUser(UserDTO userDTO)
		{
			await _userService.Update(userDTO);

			return StatusCode(201, userDTO);
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteUser(int id)
		{
			await _userService.Delete(id);

			return NoContent();
		}

	}
}
