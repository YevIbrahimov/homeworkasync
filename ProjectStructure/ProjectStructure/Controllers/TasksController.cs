﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Models;
using System.Threading.Tasks;

namespace ProjectStructure.WebApi.Controllers
{
	public class TasksController : Controller
	{
		private readonly ITaskService _taskService;
		public TasksController(ITaskService taskService)
		{
			_taskService = taskService;
		}

		[HttpGet]
		public async Task<IActionResult> GetTasks()
		{
			return Ok(await _taskService.GetAll());
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> GetTaskById(int id)
		{
			return Ok(await _taskService.Get(id));
		}

		[HttpPost]
		public async Task<IActionResult> AddTask(TaskDTO taskDTO)
		{

			await _taskService.Create(taskDTO);

			return StatusCode(201, taskDTO);
		}

		[HttpPut]
		public async Task<IActionResult> UpdateTask(TaskDTO taskDTO)
		{
			await _taskService.Update(taskDTO);

			return StatusCode(201, taskDTO);
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> Deletetask(int id)
		{
			await _taskService.Delete(id);

			return NoContent();
		}
	}
}
