﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Models;
using System.Threading.Tasks;

namespace ProjectStructure.WebApi.Controllers
{
	public class TeamsController : Controller
	{
		private readonly ITeamService _teamService;
		public TeamsController(ITeamService teamService)
		{
			_teamService = teamService;
		}

		[HttpGet]
		public async Task<IActionResult> GetTeams()
		{
			return Ok(await _teamService.GetAll());
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> GetTeamById(int id)
		{
			return Ok(await _teamService.Get(id));
		}

		[HttpPost]
		public async Task<IActionResult> AddTeam(TeamDTO teamDTO)
		{

			await _teamService.Create(teamDTO);

			return StatusCode(201, teamDTO);
		}

		[HttpPut]
		public async Task<IActionResult> UpdateTeam(TeamDTO teamDTO)
		{
			await _teamService.Update(teamDTO);

			return StatusCode(201, teamDTO);
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteTeam(int id)
		{
			await _teamService.Delete(id);

			return NoContent();
		}
	}
}
