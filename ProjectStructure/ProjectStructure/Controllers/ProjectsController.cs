﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Models;
using System.Threading.Tasks;

namespace ProjectStructure.WebApi.Controllers
{
	public class ProjectsController : Controller
	{
		private readonly IProjectService _projectService;
		public ProjectsController(IProjectService projects)
		{
			_projectService = projects;
		}

		[HttpGet]
		public async Task<IActionResult> GetProjects()
		{
			return Ok(await _projectService.GetAll());
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> GetProjectById(int id)
		{
			return Ok(await _projectService.Get(id));
		}

		[HttpPost]
		public async Task<IActionResult> AddProject(ProjectDTO projectDTO)
		{
			
			await _projectService.Create(projectDTO);

			return StatusCode(201, projectDTO);
		}

		[HttpPut]
		public async Task<IActionResult> UpdateProject(ProjectDTO projectDTO)
		{
			await _projectService.Update(projectDTO);

			return StatusCode(201, projectDTO);
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteProject(int id)
		{
			await _projectService.Delete(id);

			return NoContent();
		}
	}
}
